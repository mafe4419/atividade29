<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca - Maria Fernanda Marques</title>
        <style type="text/css">
            html {
                background-color:#8B4513;
            }

            body {
                width: 600px;
                margin: auto;
                background-color: #DEB887;
                padding: 0px 0px 20px 20px;
                border: 2px solid black;
            } 

            h1 {
                padding: 30px 0px 0px 0px;
                width: 600px;
                margin: auto;
                padding: auto;
                color: #000000;
                font-family: Trebuchet MS, sans-serif;
            }

            h4 {
                padding: 30px 20px 10px 0px;
                width: 600px;
                margin: auto;
                padding: auto;
                color: solid black; 
                font-family: Trebuchet MS, sans-serif;
            }

            button{
                background-color:#BC8F8F;
                display: block;
                margin-left: auto;
                margin-right: auto; 
                font-family: Trebuchet MS, sans-serif;               
            }

            .sla{
                text-align: center;
                border: 1px solid #eee;
            }

        </style>
    </head>
    <body>
        <h1 style = "text-align:center">Página Inicial</h1>
        <div>
            <h4 style = "text-align:center">Clique para ter acesso a Biblioteca</h4>
        </div class="sla">
        <a href="/biblioteca"> 
        <button>Acesse Aqui</button></a>
    </body>
</html>

