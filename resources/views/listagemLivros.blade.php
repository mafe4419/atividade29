<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca</title>
        <style>
            html {
                background-color: #DEB887;
            }

            body {
                width: 600px;
                margin: auto;
                background-color: #DEB887;
                padding: 0 0px 20px 20px;
                border: 2x solid black;
            } 

            h1 {
                margin: auto;
                padding: auto;
                color:  #000000;
                font-family: Trebuchet MS, sans-serif;
            }

            table{
                border-radius: 5px;
                border: black;
            }

            h3 { 
                font-family: Trebuchet MS, sans-serif;
            }

            .tab{
                background-color: #FFE4C4;
                font-family: Trebuchet MS, sans-serif;
            }

            .tab1{
                background-color: #8B4513;
                font-family: Trebuchet MS, sans-serif;
            }

        </style>
    </head>
    <body>
        <div border='4'>
            <h3 style = "text-align:center">Listagem dos Livros</h3>
            <table  border='1' width="500" align="center">
                <tr>
                    <td class="tab1" width="500">
                        <h4> ID do livro:</h4> 
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Título do livro:</h4>
                    </td>
                    <td  class="tab1" width="500"> 
                        <h4>ID do autor:</h4>
                    </td>
                    <td  class="tab1" width="500"> 
                        <h4>ID da editora:</h4>
                    </td>
                </tr>
                @foreach($livro as $livro)
                <tr>
                    <td class="tab" class="tab" width="500">
                        {{$livro->id}}
                    </td>
                    <td class="tab" width="500">
                        {{$livro->livro}}
                    </td>
                    <td  class="tab" width="500">
                        {{$livro->id_autor}}
                    </td>
                    <td class="tab" width="500">
                        {{$livro->id_editora}}
                    </td>
                </tr>
                @endforeach
            </table>
            <br>
            <h3 style = "text-align:center">Listagem das Editoras</h3>
            <table border='1' width="500" align="center">
                <tr>
                    <td class="tab1" width="500">
                        <h4>ID da editora:</h4>
                    </td>
                    <td class="tab1" width="500">
                        <h4>Nome da editora:</h4>
                    </td>
                </tr>
                @foreach($editora as $editora)
                <tr>
                    <td class="tab" width="500">
                        {{$editora->id}}
                    </td>
                    <td class="tab" width="500">
                        {{$editora->editora}}
                    </td>
                </tr>
                @endforeach
            </table>
            <br>
            <h3 style = "text-align:center">Listagem das Autores</h3>
            <table border='1' width="500" align="center">
                <tr>
                    <td  class="tab1" width="500">
                        <h4>ID do Autor:</h4>
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Nome do Autor:</h4>
                    </td>
                </tr>
                @foreach($autor as $autor)
                <tr>
                    <td  class="tab" width="500">
                        {{$autor->id}}
                    </td>
                    <td  class="tab" width="500">
                        {{$autor->autor}}
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </body>
</html>
